import amqp, { Channel, Connection } from 'amqplib';

export class Publisher {
  public send2() {
    return 'Bla bla';
  }

  public send(message: any, config: { queue: string }) {
    amqp.connect(
      `amqp://${process.env.hostname || '127.0.0.1'}:5672`,
      (connectionError: any, connection: Connection) => {
        if (connectionError) {
          throw connectionError;
        }

        connection.createChannel().then(
          (channel: Channel) => {
            channel.assertQueue(config.queue, { durable: false });
            channel.sendToQueue(config.queue, Buffer.from(message));
          },
          (channelError: any) => {
            throw channelError;
          },
        );

        setTimeout(() => {
          connection.close();
          process.exit(0);
        }, 500);
      },
    );
  }
}
