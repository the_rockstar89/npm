import amqp, { Channel, Connection } from 'amqplib';

export class Receiver {
  public subscribe(queue: string, callback: any) {
    amqp.connect(`amqp://${process.env.hostname || '127.0.0.1'}:5672`, (error: any, connection: Connection) => {
      if (error) {
        throw error;
      }

      connection.createChannel().then((channel: Channel) => {
        channel.assertQueue(queue, { durable: false });

        channel.consume(queue, callback, { noAck: false });
      });
    });
  }
}
